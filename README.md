# Laravel Vue Boilerplate

<p align="center">
  <img style="max-height:400px" src="https://i.imgur.com/wao4A4L.png">
</p>

# Features

- Laravel 6.0
- Vue + VueRouter + Vuex + ESlint
- Authentication with JWT
- Docker
- Laravel Horizon
- Laravel Telescope
- Multi-tab System
- Laravel Spatie for permissions.

# Requeriments

1. Debemos de tener instalado Docker en nuestro sistema.

Linux:

```bash
https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04
```

Windows:

```bash
https://docs.docker.com/docker-for-windows/install/
```

2. Debemos de tener instalado Docker-Compose

Linux:

```bash
https://docs.docker.com/compose/install/
```

Windows:

```bash
Instalado por defecto en Docker Desktop for Windows
```

# Installation

1. Descargamos el proyecto de nuestro repositorio.

```bash
git clone --recurse-submodules https://gitlab.zataca.es/joel/inventario.git
```

2. Accedemos a la carpeta de docker.

```bash
cd inventario/docker
```

3. Copiamos el archivo que hay en la raiz de /docker env-example a .env

4. Cambiamos la variable NODE_NGINX_VERSION

```
NODE_NGINX_VERSION=12
```

5. Creamos un volumen de datos para postgres.

```bash
docker volume create --name postgres-data -d local
```

6. Ejecutamos docker-compose para que construya nuestros contenedores (dentro de la carpeta de /docker).

```bash
sudo docker-compose up --build -d nginx postgres redis
```

7. Movemos el fichero de la raíz del proyecto .env.example a .env (copiar)

```bash
cp .env.example .env
```

8. Accedemos a la shell del contendor workspace, por comando o por la extensión de visual code al contenedor de Nginx.

```bash
sudo docker exec --user zataca -it zataca_nginx_1 bash
```

9. Instalamos las dependencias PHP del proyecto.

```bash
composer install
```

10. Ejecutamos las migraciones.

```bash
php artisan migrate
```

11. Ejecutamos los seeders para insertar datos en BD.

```bash
php artisan migrate:refresh --seed
```

12. Instalamos las dependencias de JavaScript.

```bash
yarn install
```

13. Empaquetamos las dependencias de JavaScript

```bash
yarn build
```

14. Añadimos el host que queramos al archivo /etc/hosts (ej: laravel.test )
    <img src="https://i.imgur.com/D7gyqLq.png"></img>

## Usage

#### Inicio sesión

Ya podemos acceder a la web a través bien de localhost o del host que haya definido en el fichero de hosts.

http://laravel.zataca.test

User: superadmin@zataca.com
pwd: password

#### Development

```bash
## abrimos bash de docker
sudo docker exec --user laradock -it laradock_workspace_1 bash

## accedemos a laravel-vue-boilerplate
cd laravel-vue-boilerplate

# compilamos y watcheamos.
yarn dev
```

#### Production

```bash
## abrimos bash de docker
sudo docker exec --user laradock -it laradock_workspace_1 bash

## accedemos a laravel-vue-boilerplate
cd laravel-vue-boilerplate

# empaquetamos para producción
yarn build
```

## Front-end

El front-end se compone de:

- **Vue**: Framework MVVM, SFC.
- **Vuex**: Gestión de estado, patron flux.
- **Vue-Router**: Enrutado SPA del lado del cliente.
- **SCSS**: Preprocesador de estilos.
- **HMR**: Hot Module Reloading, desarrollo mas rápido.
- **Code-Splitting**: Carga de la aplicación bajo demanda.
- **Cache-busting**: Para asegurar que se descargen los cambios en cada deploy.
- **Integración**: APIs con laravel consistente con axios y interceptores.
- **Tiempo real**: Integración de Pusher de facil configuración y captura/emisión de eventos al back-end en todo el arbol de componentes.
- **Quality Assurance**: ESLint comprueba el codigo en JavaScript, Prettier comprueba los estilos de todos los demas para que todos el equipo hable el mismo idioma. Hints durante el desarrollo y comprobación del codigo en pre-commit.
- **Otros**:
  - Captura de errores del back-end y feedback para el usuario.
  - Captura de falta de permisos con feedback para el usuario.
  - Captura de sesión expirada.
  - Notificaciones toast integradas (zero-config).
  - Menu configurable.
  - Utils para formateo de numeros y fechas (no mas momentjs).
  - Sistema de tabs, sincronizado con el historial, y con sistema de back-up.
  - CLI para automatizar tareas manuales.

### Requisitos

Aparte de instalar node, yarn, y ejecutar `yarn install`, el desarrollo en front tiene estos requisitos:

- Instalar en vscode [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint).
- Instalar en vscode [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode).
- Instalar en vscode [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur).

Esto permitira tener formateo y autocorrección de codigo para JavaScript, SASS y Vue.

> Si se va a tratar con vuex, recomendado [Vuex Suggest](https://marketplace.visualstudio.com/items?itemName=mishannn.vuex-suggest).

### Componentes

Los componentes se encuentran en '/app-front/components/' y se dividen en 2 partes:

- **ui**: Componentes comunes de la interfaz (botones, layouts, inputs, etc).
- **pages**: Las paginas y secciones de la aplicación (nuestras "vistas").

### Crear una nueva sección

> Nuestra sección nueva para el ejemplo se llamará **user**.

1.  **Crear el componente principal y la ruta**:
    Usamos el la herramienta de CLI `yarn front` para crear nuestra vista y su ruta:

    ```bash
    # Quedaría asi
    yarn front
    ? Elige lo que quieras hacer: Crear una vista
    ? Nombre del componente: User # Siempre tiene que ser en UpperCamelCase
    ? Ruta de la vista (ej: /user/): /user

    # Si todo a ido bien:
    ✔ success  Vista creada en [...ruta del proyecto]/app-front/components/pages/user/user-page.vue
    ✔ success  Ruta creada en [...ruta del proyecto]/app-front/components/pages/user/route.js
    ```

    > Si el CLI detecta que ya existe la carpeta donde se va a generar, no continuara hasta que no se elliga una que no existe.

2.  **Y ya estaria**. Nuestra nueva sección esta disponible desde la ruta que hayamos elegido.

### Namespaces

Para evitar el infierno de rutas relativas, se han preparado una serie de namespaces para acelar la importacion de modulos/componentes/recursos, etc:

| Alias             | Direccion              |
| ----------------- | ---------------------- |
| **`$network`**    | `app-front/network`    |
| **`$components`** | `app-front/components` |
| **`$mixins`**     | `app-front/mixins`     |
| **`$resources`**  | `app-front/resources`  |
| **`$lib`**        | `app-front/lib`        |

```js
// Mal
import * as api from '../../../../network/api.js';
import UiButton from '../../components/ui/button.vue';

// Bien
import * as api from '$network/api.js';
import UiButton from '$components/ui/button.vue';
```

### Tabs

Debido a que el sistema debe conocer que componentes necesitan una correcta gestión de tabs (mantenerse en segundo plano al cambiar de tab, etc), es necesario que las paginas (vease [Crear una nueva seccion](#crear-una-nueva-seccion)), incluyan un **mixin**:

```html
<!-- Nuestro componente /app-front/components/pages/user/user-page.vue -->

<script>
  import pageMixin from '$mixins/page-mixin.js'; // Importamos el mixin

  export default {
    name: 'User',
    mixins: [
      pageMixin // Lo incluimos en el componente
    ],
    ...
  }
</script>
```

Con esto, el mixin ya se ocupara de gestionar la cache del componente.

> Hay que tener en cuenta que es **obligatorio** que los componentes que usen este mixin tengan definida la propiedad _name_.

### Layouts

Todas las vistas por defecto estan totalmente vacias, pudiendo insertar el contenido que queramos.
Si queremos usar el layout principal de la plantilla (header, menu lateral, etc) hay que usar el componente `<TabsLayout>`:

```html
<template>
  <!-- Englobamos toda nuestra plantilla dentro del layout -->
  <TabsLayout>
    ...
  </TabsLayout>
</template>

<script>
  // Importamos el componente
  import TabsLayout from '$compontents/ui/tabs-layout.vue';

  export default {
    name: 'User',
    components: {
      TabsLayout // Declaramos el componente
    }
    ...
  }
</script>
```
