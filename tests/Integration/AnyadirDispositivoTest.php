<?php

namespace Tests\Integration;

use App\User;
use App\Dispositivo;
use Illuminate\Container\Container;
use Inventario\Dispositivos\Application\AnyadirDispositivo;
use Inventario\Dispositivos\Domain\DispositivosRepositoryInterface;
use Inventario\Dispositivos\Infrastructure\DispositivosRepository;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Tests\TestCase;

class AnyadirDispositivoTest extends TestCase
{
    // use MockeryPHPUnitIntegration;

    /** @test*/
    function anyadir_un_dispositivo()
    {
        $user = User::find(2);

        $container = Container::getInstance();
        $command = $container->make(AnyadirDispositivo::class);

        $command->run('Movil', 'Smartphone', $user->id);

        $expect = [
            'nombre' => 'Movil',
            'tipo' => 'Smartphone',
            'userId' => $user->id
        ];
        $this->assertDatabaseHas('dispositivos', $expect);
    }

    /** @test*/
    function anyadir_dispositivo_con_trait()
    {
        $user = User::find(2);

        $mockRepository = Mockery::mock(DispositivosRepository::class); //->makePartial(); //Dobla de forma parcial la clase, es decir, solo dobla los métodos para los cuales se crean expectativas, el resto de métodos son los reales
        $mockRepository->shouldReceive('anyadirDispositivo')        //Dobla el método anyadirDispositivo
                        ->with('Movil', 'Smartphone', $user->id)    //Verficamos que se llama exactamente con unos argumentos concretos
                        ->andReturn(true)                           //Devolvemos una respuesta enlatada
                        ->once();                                //Verifica que se llama al método al menos una vez

        $command = new AnyadirDispositivo($mockRepository);

        // $resultado = 25;
        $resultado = $command->run('Movil', 'Smartphone', $user->id);

        // $this->assertTrue($resultado);
        $this->assertEquals(25, $resultado);
    }
}
