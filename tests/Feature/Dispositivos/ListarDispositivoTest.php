<?php

namespace Tests\Feature\Dispositivos;

use App\Dispositivo;
use ArgumentCountError;
use Illuminate\Container\Container;
use Illuminate\Database\QueryException;
use Inventario\Dispositivos\Application\ListarDispositivo;
use Tests\TestCase;

class ListarDispositivoTest extends TestCase
{

    /** @test*/
    function listar_un_dispositivo()
    {

        $dispositivo = factory(Dispositivo::class)->create(['nombre' => 'Movil', 'tipo' => 'smartphone']);
        $container = Container::getInstance();
        $command = $container->make(ListarDispositivo::class);
        $result = $command->run($dispositivo->id);

        $this->assertEquals('Movil', $result->nombre);
        $this->assertEquals('smartphone', $result->tipo);
        $this->assertEquals(4, $result->userId);
    }

    /** @test*/
    function listar_dispositivos_con_id_no_valido()
    {
        $this->expectException(QueryException::class);

        $container = Container::getInstance();
        $command = $container->make(ListarDispositivo::class);

        $command->run('dfgh');
    }
}
