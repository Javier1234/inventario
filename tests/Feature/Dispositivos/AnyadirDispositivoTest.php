<?php

namespace Tests\Feature\Dispositivos;

use App\User;
use ArgumentCountError;
use Illuminate\Container\Container;
use Illuminate\Database\QueryException;
use Inventario\Dispositivos\Application\AnyadirDispositivo;
use Tests\TestCase;

class AnyadirDispositivoTest extends TestCase
{

    /** @test*/
    function anyadir_un_dispositivo()
    {
        $user = factory(User::class)->create();

        $container = Container::getInstance();
        $command = $container->make(AnyadirDispositivo::class);

        $command->run('Movil', 'Smartphone', $user->id);

        $expect = [
            'nombre' => 'Movil',
            'tipo' => 'Smartphone',
            'userId' => $user->id
        ];

        $this->assertDatabaseHas('dispositivos', $expect);
    }

    /** @test*/
    function lanza_excepcion_cuando_no_le_pasamos_el_usuario()
    {
        // $user = factory(User::class)->create();

        $this->expectException(QueryException::class);

        $container = Container::getInstance();
        $command = $container->make(AnyadirDispositivo::class);

        $command->run('Movil', 'Smartphone', -1);
    }

    /** @test*/
    function lanza_excepcion_cuando_no_le_pasamos_el_numero_corrector_de_argumentos()
    {
        $user = factory(User::class)->create();

        $this->expectException(ArgumentCountError::class);

        $container = Container::getInstance();
        $command = $container->make(AnyadirDispositivo::class);

        $command->run('Movil', 'Smartphone');
    }

    /** @test*/
    function lanza_excepcion_cuando_no_le_pasamos_argumentos_de_tipo_no_valido()
    {
        $this->expectException(QueryException::class);

        $container = Container::getInstance();
        $command = $container->make(AnyadirDispositivo::class);

        $command->run('Movil', 'Smartphone', 'afdsfa');
    }
}
