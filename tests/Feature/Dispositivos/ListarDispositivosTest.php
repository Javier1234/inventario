<?php

namespace Tests\Feature\Dispositivos;

use App\Dispositivo;
use ArgumentCountError;
use Illuminate\Container\Container;
use Illuminate\Database\QueryException;
use Inventario\Dispositivos\Application\ListarDispositivos;
use Tests\TestCase;

class ListarDispositivosTest extends TestCase
{

    /** @test*/
    function listar_dispositivos()
    {

        $dispositivo = factory(Dispositivo::class, 10)->create();
        $container = Container::getInstance();
        $command = $container->make(ListarDispositivos::class);
        $result = $command->run();
        $this->assertCount(10, $result);
    }
}
