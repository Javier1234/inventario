<?php

namespace Tests\Feature\Dispositivos;

use Inventario\Dispositivos\Exception\BorrarDispositivoException;
use Inventario\Dispositivos\Exception;
use App\Dispositivo;
use ArgumentCountError;
use Illuminate\Container\Container;
use Inventario\Dispositivos\Application\BorrarDispositivos;
use Tests\TestCase;

class BorrarDispositivoTest extends TestCase
{
    /** @test*/
    function borrar_un_dispositivo()
    {
        $dispositivo = factory(Dispositivo::class)->create();
        $container = Container::getInstance();
        $command = $container->make(BorrarDispositivos::class);

        $expect = [
            'nombre' => $dispositivo->nombre,
            'tipo' => $dispositivo->tipo,
            'userId' => $dispositivo->userId
        ];

        $this->assertDatabaseHas('dispositivos', $expect);
        $command->run($dispositivo->id);

        $this->assertDatabaseMissing('dispositivos', $expect);
    }
    /** @test*/
    function lanza_excepcion_cuando_no_existe_el_dispositivo()
    {
        $this->expectException(BorrarDispositivoException::class);
        $this->expectExceptionMessage(BorrarDispositivoException::class);

        $dispositivo = factory(Dispositivo::class)->create();
        $container = Container::getInstance();
        $command = $container->make(BorrarDispositivos::class);
        $command->run(4);
    }
}
