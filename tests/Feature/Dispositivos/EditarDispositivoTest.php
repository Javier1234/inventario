<?php

namespace Tests\Feature\Dispositivos;

use App\User;
use App\Dispositivo;
use ArgumentCountError;
use Illuminate\Container\Container;
use Illuminate\Database\QueryException;
use Inventario\Dispositivos\Application\EditarDispositivo;
use Tests\TestCase;

class EditarDispositivoTest extends TestCase
{

    /** @test*/
    function editar_un_dispositivo()
    {
        $user = factory(User::class)->create();
        $dispositivo = factory(Dispositivo::class)->create();
        $container = Container::getInstance();
        $command = $container->make(EditarDispositivo::class);

        $command->run($dispositivo->id, 'Tablet', 'Smartphone', $user->id);

        $expect = [
            'id' => $dispositivo->id,
            'nombre' => 'Tablet',
            'tipo' => 'Smartphone',
            'userId' => $user->id
        ];

        $this->assertDatabaseHas('dispositivos', $expect);
    }

    /** @test*/
    function lanza_excepcion_cuando_no_le_pasamos_el_usuario_a_editar()
    {
        // $user = factory(User::class)->create();

        $this->expectException(QueryException::class);
        $dispositivo = factory(Dispositivo::class)->create();

        $container = Container::getInstance();
        $command = $container->make(EditarDispositivo::class);

        $command->run($dispositivo->id, 'Movil', 'Smartphone', -1);
    }

    /** @test*/
    function lanza_excepcion_cuando_no_le_pasamos_el_numero_corrector_de_argumentos_a_editar()
    {
        $user = factory(User::class)->create();

        $this->expectException(ArgumentCountError::class);

        $container = Container::getInstance();
        $command = $container->make(EditarDispositivo::class);

        $command->run('Movil', 'Smartphone');
    }

    /** @test*/
    function lanza_excepcion_cuando_le_pasamos_argumentos_de_tipo_no_valido_a_editar()
    {
        $this->expectException(QueryException::class);
        $dispositivo = factory(Dispositivo::class)->create();

        $container = Container::getInstance();
        $command = $container->make(EditarDispositivo::class);

        $command->run($dispositivo->id, 'Movil', 'Smartphone', 'afdsfa');
    }
}
