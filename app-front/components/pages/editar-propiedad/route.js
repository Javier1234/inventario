export default {
  name: 'Editar Propiedad',
  path: '/editar-propiedad/:id',
  component: () => import('./editar-propiedad-page.vue')
};
