export default {
  name: 'Editar Usuario',
  path: '/editar-usuario/:id',
  component: () => import('./editar-usuario-page.vue')
};
