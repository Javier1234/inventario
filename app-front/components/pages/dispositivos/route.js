export default {
  name: 'Dispositivos',
  path: '/dispositivos',
  component: () => import('./dispositivos-page.vue')
};
