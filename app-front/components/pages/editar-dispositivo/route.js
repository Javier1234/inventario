export default {
  name: 'Editar Dispositivo',
  path: '/editar-dispositivo/:id',
  component: () => import('./editar-dispositivo-page.vue')
};
