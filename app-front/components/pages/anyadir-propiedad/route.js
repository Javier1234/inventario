export default {
  name: 'Añadir Propiedad',
  path: '/anyadir-propiedad/:id',
  component: () => import('./anyadir-propiedad-page.vue')
};
