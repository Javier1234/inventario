export default {
  name: 'Listar Usuarios',
  path: '/listar-usuarios',
  component: () => import('./listar-usuarios-page.vue')
};
