// Aqui estan todas las peticiones al backend.
// Se encapsulan aqui para agilizar el uso de interceptors, compartir cabeceras, etc.

// Aqui no debe haber logica. Es un puente entre el cliente y el back-end.

import axios from 'axios';
import errorNotifications from '$lib/axios-interceptors/error-notifications-interceptor.js';
import { injectHeader, storeToken } from '$lib/axios-interceptors/dev-token-support-interceptor.js';

const { VUE_APP_URL } = process.env;

if (process.env.NODE_ENV === 'development') {
  injectHeader(axios);
  storeToken(axios);
}

errorNotifications(axios);

export const login = body => axios.post(`${VUE_APP_URL}/api/login`, body);
export const logout = () => axios.post(`${VUE_APP_URL}/api/logout`);
export const fetchUser = () => axios.get(`${VUE_APP_URL}/api/user`);
export const ListarDispositivos = () => axios.get(`${VUE_APP_URL}/api/dispositivos`);
export const ListarDispositivo = id => axios.get(`${VUE_APP_URL}/api/dispositivos/${id}`);
export const EditarDispositivo = (json, id) =>
  axios.put(`${VUE_APP_URL}/api/dispositivos/${id}`, json);
export const BorrarDispositivo = id => axios.delete(`${VUE_APP_URL}/api/dispositivos/${id}`);
export const AnyadirDispositivo = json => axios.post(`${VUE_APP_URL}/api/dispositivos`, json);

export const ListarPropiedades = id => axios.get(`${VUE_APP_URL}/api/propiedades/${id}`);
export const ListarPropiedad = id => axios.get(`${VUE_APP_URL}/api/propiedad/${id}`);
export const EditarPropiedad = (json, id) => axios.put(`${VUE_APP_URL}/api/propiedades/${id}`, json);
export const BorrarPropiedad = id => axios.delete(`${VUE_APP_URL}/api/propiedades/${id}`);
export const AnyadirPropiedad = json => axios.post(`${VUE_APP_URL}/api/propiedades`, json);

export const ListarUsuariosSelect = () => axios.get(`${VUE_APP_URL}/api/usuariosSelect`);
export const ListarUsuarios = () => axios.get(`${VUE_APP_URL}/api/usuarios`);
export const ListarUsuario = id => axios.get(`${VUE_APP_URL}/api/usuario/${id}`);
export const EditarUsuario = (json, id) => axios.put(`${VUE_APP_URL}/api/usuarios/${id}`, json);
export const BorrarUsuario = id => axios.delete(`${VUE_APP_URL}/api/usuarios/${id}`);
export const AnyadirUsuario = json => axios.post(`${VUE_APP_URL}/api/usuarios`, json);
