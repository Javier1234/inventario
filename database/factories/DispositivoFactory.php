<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Dispositivo;
use Faker\Generator as Faker;

$factory->define(Dispositivo::class, function (Faker $faker) {
    return [
        'nombre' => 'smartphone',
        'tipo' => 'movil',
        'userId' => '1'//Random entre 1 y 11
    ];
});
