<?php
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class ManageRolesAndUsersPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::firstOrCreate(['name' => 'admin']);
        Role::firstOrCreate(['name' => 'user']);

        Permission::findOrCreate('manage_users');
        // Sync will delete then re-add all the permissions.
        $admin->syncPermissions([
            'manage_users',
        ]);
    }
}
