<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DispositivosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dispositivos')->insert([
            'nombre' => 'PC',
            'tipo' => 'Portatil',
            'userId' => rand(1, 10),
        ]);
        DB::table('dispositivos')->insert([
            'nombre' => 'PC',
            'tipo' => 'Ordenador sobremesa',
            'userId' => rand(1, 10),
        ]);
        DB::table('dispositivos')->insert([
            'nombre' => 'Teclado',
            'tipo' => 'Teclado inalambrico',
            'userId' => rand(1, 10),
        ]);
        DB::table('dispositivos')->insert([
            'nombre' => 'Auriculares',
            'tipo' => 'Auriculares bluetooth',
            'userId' => rand(1, 10),
        ]);
        DB::table('dispositivos')->insert([
            'nombre' => 'Ratón',
            'tipo' => 'Ratón inalambrico',
            'userId' => rand(1, 10),
        ]);
    }
}
