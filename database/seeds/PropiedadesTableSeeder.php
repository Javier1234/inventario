<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PropiedadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('propiedades')->insert([
            'nombre' => 'RAM',
            'propiedad' => '8GB',
            'dispositivoId' => '1',
        ]);
        DB::table('propiedades')->insert([
            'nombre' => 'Disco Duro',
            'propiedad' => '256GB SSD',
            'dispositivoId' => '1',
        ]);
        DB::table('propiedades')->insert([
            'nombre' => 'RAM',
            'propiedad' => '16GB',
            'dispositivoId' => '2',
        ]);
        DB::table('propiedades')->insert([
            'nombre' => 'Disco Duro',
            'propiedad' => '500GB SSD',
            'dispositivoId' => '2',
        ]);
        DB::table('propiedades')->insert([
            'nombre' => 'Bluetooth',
            'propiedad' => '5.0',
            'dispositivoId' => '4',
        ]);
    }
}
