<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if ($this->shouldSeedsBeRun()) {
            //Desactivar las restricciones de integridad
            DB::statement('SET session_replication_role = \'replica\';');

            $this->truncateTables([
                'roles',
                'users',
                'permissions'
            ]);

            $this->call(ManageRolesAndUsersPermissionSeeder::class);

            $this->call(UsersTableSeeder::class);
            $this->call(DispositivosTableSeeder::class);
            $this->call(PropiedadesTableSeeder::class);

            DB::statement('SET session_replication_role = \'origin\';');
        }
    }

    public function truncateTables(array $tables)
    {
        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }
    }

    protected function shouldSeedsBeRun()
    {
        if (app()->environment() === 'production') {
            $this->command->getOutput()->writeln(
                "<error>This seeder cannot be run in production</error>"
            );
            return false;
        }

        return true;
    }
}
