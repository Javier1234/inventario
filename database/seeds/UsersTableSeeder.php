<?php

use App\DatagridModel;
use Silber\Bouncer\Bouncer;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Getting existing roles
        $roles = DB::table("roles")->get();

        factory(App\User::class, 1)->create()->each(function ($user) use ($roles) {

            $user->name = "SuperAdmin";
            $user->email = "superadmin@zataca.com";
            $user->phone_number = "661637458";

            $user->save();

            $user->assignRole('admin');
        });

        factory(App\User::class, 10)->create()->each(function ($user) use ($roles) {

            $user->save();

            $user->assignRole($roles->random()->name);
        });
    }
}
