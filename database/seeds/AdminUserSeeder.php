<?php
use Illuminate\Database\Seeder;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = App\User::updateOrCreate([
            'name' => 'SuperAdmin',
            'email' => 'superadmin@zataca.com'
        ]);

        $admin->assignRole('admin');
    }
}
