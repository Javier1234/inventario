<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('apellidos')->nullable();
            $table->string('fechaNacimiento')->nullable();
            $table->string('generoLegal')->nullable();
            $table->string('nacionalidad')->nullable();
            $table->string('numeroCuentaBancaria')->unique()->nullable();
            $table->string('documentoIdentidad')->unique()->nullable();
            $table->string('numeroSeguridadSocial')->unique()->nullable();
            $table->string('direccion')->nullable();
            $table->string('ciudad')->nullable();
            $table->integer('codigoPostal')->nullable();
            $table->string('provincia')->nullable();
            $table->string('pais')->nullable();
            $table->string('nombreContactoEmergencia')->nullable();
            $table->integer('telefonoContactoEmergencia')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
