@extends('layout')

@section('content')
    <h1> {{$title}} </h1>
    <ul>
        @forelse ($users as $user)
            <li>{{ $user }}</li>
        @empty
             No hay usuarios registrados.
        @endforelse
         
    </ul>

    @endsection

    @section('sidebar')
        
            <h2>Barra lateral personalizada</h2>
    @endsection

    @section('title', "Usuarios")
        
    

