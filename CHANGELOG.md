# Changelog

## 0.01 - 2020-01-23

- Instalación de Laravel 6.0.
- Configuración de JWT.
- Configuración de Docker.