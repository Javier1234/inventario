<?php

namespace Inventario\Dispositivos\Application;

use Inventario\Dispositivos\Domain\DispositivosRepositoryInterface;

class EditarDispositivo
{
    protected $logicaDispositivos;


    public function __construct(DispositivosRepositoryInterface $logicaDispositivos)
    {
        $this->logicaDispositivos = $logicaDispositivos;
    }
    public function run($idDispositivo, $nombre, $tipo, $userId)
    {
        $this->logicaDispositivos->editarDispositivo($idDispositivo, $nombre, $tipo, $userId);
    }
}
