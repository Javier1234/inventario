<?php

namespace Inventario\Dispositivos\Application;

use Inventario\Dispositivos\Domain\DispositivosRepositoryInterface;

class ListarDispositivo
{
    protected $logicaDispositivos;


    public function __construct(DispositivosRepositoryInterface $logicaDispositivos)
    {
        $this->logicaDispositivos = $logicaDispositivos;
    }
    public function run($id)
    {
        return $this->logicaDispositivos->listarDispositivo($id);
    }
}
