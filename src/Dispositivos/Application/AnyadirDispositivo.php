<?php

namespace Inventario\Dispositivos\Application;

use Inventario\Dispositivos\Domain\DispositivosRepositoryInterface;

class AnyadirDispositivo
{
    protected $logicaDispositivos;


    public function __construct(DispositivosRepositoryInterface $logicaDispositivos)
    {
        $this->logicaDispositivos = $logicaDispositivos;
    }
    public function run($nombre, $tipo, $userId)
    {
        return $this->logicaDispositivos->anyadirDispositivo($nombre, $tipo, $userId);
    }
}
