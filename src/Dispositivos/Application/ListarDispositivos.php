<?php

namespace Inventario\Dispositivos\Application;

use Inventario\Dispositivos\Domain\DispositivosRepositoryInterface;

class ListarDispositivos
{
    protected $logicaDispositivos;

    public function __construct(DispositivosRepositoryInterface $logicaDispositivos)
    {
        $this->logicaDispositivos = $logicaDispositivos;
    }
    public function run()
    {
        return $this->logicaDispositivos->listarDispositivos();
    }
}
