<?php

namespace Inventario\Dispositivos\Domain;

interface DispositivosRepositoryInterface
{
    public function listarDispositivos();

    public function listarDispositivo($id);

    public function borrarDispositivos($id);

    public function anyadirDispositivo($nombre, $tipo, $userId);

    public function editarDispositivo($idDispositivo, $nombre, $tipo, $userId);
}
