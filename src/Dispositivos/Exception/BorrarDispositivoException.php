<?php

namespace Inventario\Dispositivos\Exception;

class BorrarDispositivoException extends \RuntimeException implements DispositivoException
{
    protected $id;

    public static function forId($id, $message)
    {
        $ex = new self($message);
        $ex->id = $id;
        return $ex;
    }
    public function getId()
    {
        return $this->id;
    }
}
