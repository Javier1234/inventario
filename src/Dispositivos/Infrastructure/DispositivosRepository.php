<?php

namespace Inventario\Dispositivos\Infrastructure;

use Inventario\Dispositivos\Exception\BorrarDispositivoException;
use Inventario\Dispositivos\Domain\DispositivosRepositoryInterface;
use App\Dispositivo;
use App\Propiedad;

class DispositivosRepository implements DispositivosRepositoryInterface
{

    public function listarDispositivos()
    {
        $dispositivos = Dispositivo::select('dispositivos.id', 'dispositivos.nombre', 'dispositivos.tipo', 'dispositivos.userId', 'users.name')
        ->join('users', 'dispositivos.userId', '=', 'users.id')
        ->get();

        return $dispositivos;
    }

    public function listarDispositivo($id)
    {

        return Dispositivo::where('id', $id)
            ->first();
    }


    public function borrarDispositivos($id)
    {
        try {
            $propiedades = Propiedad::where('dispositivoId', '=', $id);
            $propiedades->delete();
            $dispositivo = Dispositivo::findOrFail($id);
            $dispositivo->delete();
        } catch (\Throwable $th) {
            throw BorrarDispositivoException::forId($id, $th->getMessage());
        }
    }
    public function anyadirDispositivo($nombre, $tipo, $userId)
    {
        $dispositivo = new Dispositivo();
        $dispositivo->nombre = $nombre;
        $dispositivo->tipo = $tipo;
        $dispositivo->userId = $userId;
        $dispositivo->save();
    }

    public function editarDispositivo($idDispositivo, $nombre, $tipo, $userId)
    {

        $dispositivo = Dispositivo::find($idDispositivo);
        $dispositivo->nombre = $nombre;
        $dispositivo->tipo = $tipo;
        $dispositivo->userId = $userId;
        $dispositivo->save();
    }
}
