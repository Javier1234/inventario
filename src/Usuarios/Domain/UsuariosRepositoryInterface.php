<?php

namespace Inventario\Usuarios\Domain;

interface UsuariosRepositoryInterface
{
    public function listarUsuarios();

    public function listarUsuario($id);

    public function borrarUsuarios($id);

    public function anyadirUsuario($arrayDatosUsuario);

    public function editarUsuario($arrayDatosUsuario);
}
