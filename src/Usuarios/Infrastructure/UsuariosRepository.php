<?php

namespace Inventario\Usuarios\Infrastructure;

use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Inventario\Usuarios\Domain\UsuariosRepositoryInterface;
use App\User;
use App\Dispositivo;

class UsuariosRepository implements UsuariosRepositoryInterface
{

    public function listarUsuarios()
    {
        $usuarios = User::select('*')->get()->toArray();

        return $usuarios;
    }

    public function listarUsuario($id)
    {

        return User::where('id', $id)
            ->first();
    }


    public function borrarUsuarios($id)
    {
        $dispositivo = Dispositivo::where('userId', '=', $id)->count();
        if ($dispositivo == 0) {
            $usuario = User::findOrFail($id);
            $usuario->delete();
            return true;
        } else {
            return false;
        }
    }
    public function anyadirUsuario($arrayDatosUsuario)
    {

        $usuario = new User();
        $usuario->name = $arrayDatosUsuario['name'];
        $usuario->email = $arrayDatosUsuario['email'];
        $usuario->description = $arrayDatosUsuario['description'];
        $usuario->phone_number = $arrayDatosUsuario['phone_number'];
        $usuario->password = bcrypt($arrayDatosUsuario['password']);
        $usuario->apellidos = $arrayDatosUsuario['apellidos'];
        $usuario->fechaNacimiento = $arrayDatosUsuario['fechaNacimiento'];
        $usuario->generoLegal = $arrayDatosUsuario['generoLegal'];
        $usuario->nacionalidad = $arrayDatosUsuario['nacionalidad'];
        $usuario->numeroCuentaBancaria = $arrayDatosUsuario['numeroCuentaBancaria'];
        $usuario->documentoIdentidad = $arrayDatosUsuario['documentoIdentidad'];
        $usuario->numeroSeguridadSocial = $arrayDatosUsuario['numeroSeguridadSocial'];
        $usuario->direccion = $arrayDatosUsuario['direccion'];
        $usuario->ciudad = $arrayDatosUsuario['ciudad'];
        $usuario->codigoPostal = $arrayDatosUsuario['codigoPostal'];
        $usuario->provincia = $arrayDatosUsuario['provincia'];
        $usuario->pais = $arrayDatosUsuario['pais'];
        $usuario->nombreContactoEmergencia = $arrayDatosUsuario['nombreContactoEmergencia'];
        $usuario->telefonoContactoEmergencia = $arrayDatosUsuario['telefonoContactoEmergencia'];
        $usuario->save();

        event(new Registered($usuario));
    }

    public function editarUsuario($arrayDatosUsuario)
    {
        $usuario = User::find($arrayDatosUsuario['id']);
        $usuario->name = $arrayDatosUsuario['name'];
        $usuario->email = $arrayDatosUsuario['email'];
        $usuario->description = $arrayDatosUsuario['description'];
        $usuario->phone_number = $arrayDatosUsuario['phone_number'];
        $usuario->apellidos = $arrayDatosUsuario['apellidos'];
        $usuario->fechaNacimiento = $arrayDatosUsuario['fechaNacimiento'];
        $usuario->generoLegal = $arrayDatosUsuario['generoLegal'];
        $usuario->nacionalidad = $arrayDatosUsuario['nacionalidad'];
        $usuario->numeroCuentaBancaria = $arrayDatosUsuario['numeroCuentaBancaria'];
        $usuario->documentoIdentidad = $arrayDatosUsuario['documentoIdentidad'];
        $usuario->numeroSeguridadSocial = $arrayDatosUsuario['numeroSeguridadSocial'];
        $usuario->direccion = $arrayDatosUsuario['direccion'];
        $usuario->ciudad = $arrayDatosUsuario['ciudad'];
        $usuario->codigoPostal = $arrayDatosUsuario['codigoPostal'];
        $usuario->provincia = $arrayDatosUsuario['provincia'];
        $usuario->pais = $arrayDatosUsuario['pais'];
        $usuario->nombreContactoEmergencia = $arrayDatosUsuario['nombreContactoEmergencia'];
        $usuario->telefonoContactoEmergencia = $arrayDatosUsuario['telefonoContactoEmergencia'];
        $usuario->save();
    }
}
