<?php

namespace Inventario\Usuarios\Application;

use Inventario\Usuarios\Domain\UsuariosRepositoryInterface;

class EditarUsuarioCommand
{
    protected $logicaUsuarios;


    public function __construct(UsuariosRepositoryInterface $logicaUsuarios)
    {
        $this->logicaUsuarios = $logicaUsuarios;
    }
    public function run($arrayDatosUsuario)
    {
        $this->logicaUsuarios->editarUsuario($arrayDatosUsuario);
    }
}
