<?php

namespace Inventario\Usuarios\Application;

use Inventario\Usuarios\Domain\UsuariosRepositoryInterface;

class AnyadirUsuarioCommand
{
    protected $logicaUsuarios;

    public function __construct(UsuariosRepositoryInterface $logicaUsuarios)
    {
        $this->logicaUsuarios = $logicaUsuarios;
    }
    public function run($arrayDatosUsuario)
    {
        return $this->logicaUsuarios->anyadirUsuario($arrayDatosUsuario);
    }
}
