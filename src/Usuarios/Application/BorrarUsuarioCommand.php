<?php

namespace Inventario\Usuarios\Application;

use Inventario\Usuarios\Domain\UsuariosRepositoryInterface;

class BorrarUsuarioCommand
{
    protected $logicaUsuarios;

    public function __construct(UsuariosRepositoryInterface $logicaUsuarios)
    {
        $this->logicaUsuarios = $logicaUsuarios;
    }
    public function run($id)
    {
        return $this->logicaUsuarios->borrarUsuarios($id);
    }
}
