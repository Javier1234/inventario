<?php

namespace Inventario\Usuarios\Application;

use Inventario\Usuarios\Domain\UsuariosRepositoryInterface;

class ListarUsuariosCommand
{
    protected $logicaUsuarios;

    public function __construct(UsuariosRepositoryInterface $logicaUsuarios)
    {
        $this->logicaUsuarios = $logicaUsuarios;
    }
    public function run()
    {
        return $this->logicaUsuarios->listarUsuarios();
    }
}
