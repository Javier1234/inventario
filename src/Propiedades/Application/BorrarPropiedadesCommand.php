<?php

namespace Inventario\Propiedades\Application;

use Inventario\Propiedades\Domain\PropiedadesRepositoryInterface;

class BorrarPropiedadesCommand
{
    protected $logicaPropiedades;

    public function __construct(PropiedadesRepositoryInterface $logicaPropiedades)
    {
        $this->logicaPropiedades = $logicaPropiedades;
    }
    public function run($id)
    {
        return $this->logicaPropiedades->borrarPropiedades($id);
    }
}
