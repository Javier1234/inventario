<?php

namespace Inventario\Propiedades\Application;

use Inventario\Propiedades\Domain\PropiedadesRepositoryInterface;

class AnyadirPropiedadCommand
{
    protected $logicaPropiedades;


    public function __construct(PropiedadesRepositoryInterface $logicaPropiedades)
    {
        $this->logicaPropiedades = $logicaPropiedades;
    }
    public function run($nombre, $propiedad, $dispositivoId)
    {
        return $this->logicaPropiedades->anyadirPropiedad($nombre, $propiedad, $dispositivoId);
    }
}
