<?php

namespace Inventario\Propiedades\Application;

use Inventario\Propiedades\Domain\PropiedadesRepositoryInterface;

class EditarPropiedadCommand
{
    protected $logicaPropiedades;


    public function __construct(PropiedadesRepositoryInterface $logicaPropiedades)
    {
        $this->logicaPropiedades = $logicaPropiedades;
    }
    public function run($idPropiedad, $nombre, $propiedad, $dispositivoId)
    {
        $this->logicaPropiedades->editarPropiedad($idPropiedad, $nombre, $propiedad, $dispositivoId);
    }
}
