<?php

namespace Inventario\Propiedades\Domain;

interface PropiedadesRepositoryInterface
{
    public function listarPropiedades($id);

    public function listarPropiedad($id);

    public function borrarPropiedades($id);

    public function anyadirPropiedad($nombre, $propiedad, $dispositivoId);

    public function editarPropiedad($idPropiedad, $nombre, $propiedad, $dispositivoId);
}
