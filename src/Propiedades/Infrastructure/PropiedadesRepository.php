<?php

namespace Inventario\Propiedades\Infrastructure;

use Inventario\Propiedades\Domain\PropiedadesRepositoryInterface;
use App\Propiedad;

class PropiedadesRepository implements PropiedadesRepositoryInterface
{

    public function listarPropiedades($id)
    {
        $propiedades = Propiedad::select('propiedades.id', 'propiedades.nombre', 'propiedades.propiedad')
        ->where('propiedades.dispositivoId', '=', $id)
        ->get();

        return $propiedades;
    }

    public function listarPropiedad($id)
    {

        return Propiedad::where('id', $id)
            ->first();
    }


    public function borrarPropiedades($id)
    {
        $propiedad = Propiedad::findOrFail($id);
        $propiedad->delete();
    }
    public function anyadirPropiedad($nombre, $propiedad, $dispositivoId)
    {
        $propObjeto = new Propiedad();
        $propObjeto->nombre = $nombre;
        $propObjeto->propiedad = $propiedad;
        $propObjeto->dispositivoId = $dispositivoId;
        $propObjeto->save();
    }

    public function editarPropiedad($idPropiedad, $nombre, $propiedad, $dispositivoId)
    {

        $propObjeto = Propiedad::find($idPropiedad);
        $propObjeto->nombre = $nombre;
        $propObjeto->propiedad = $propiedad;
        $propObjeto->dispositivoId = $dispositivoId;
        $propObjeto->save();
    }
}
