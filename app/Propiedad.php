<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Propiedad extends Model
{
    protected $table = 'propiedades';

    protected $primaryKey = 'id';

    public function dispositivo()
    {
        return $this->belongsTo(Dispositivo::class);
    }
}
