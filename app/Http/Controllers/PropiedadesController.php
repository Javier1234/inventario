<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inventario\Propiedades\Application\ListarPropiedadesCommand;
use Inventario\Propiedades\Application\ListarPropiedadCommand;
use Inventario\Propiedades\Application\AnyadirPropiedadCommand;
use Inventario\Propiedades\Application\EditarPropiedadCommand;
use Inventario\Propiedades\Application\BorrarPropiedadesCommand;

class PropiedadesController extends Controller
{
    public function index(ListarPropiedadesCommand $listarPropiedades, $id) //INYECCION DE DEPENDENCIAS
    {
        $propiedades = $listarPropiedades->run($id);
        $json = [];

        foreach ($propiedades as $propiedad) {
            $json[] = [
                'id' => $propiedad->id,
                'nombre' => $propiedad->nombre,
                'propiedad' => $propiedad->propiedad
            ];
        }

        return response()->api($json)->setStatusCode(200);
    }
    public function store(AnyadirPropiedadCommand $anyadirPropiedad, Request $request)
    {
        $nombre = $request->nombre;
        $propiedad = $request->propiedad;
        $dispositivoId = $request->dispositivoId;
        $anyadirPropiedad->run($nombre, $propiedad, $dispositivoId);
    }
    public function show(ListarPropiedadCommand $listarPropiedad, $id)
    {
        $propiedad = $listarPropiedad->run($id);
        return response()->api($propiedad)->setStatusCode(200);
    }

    public function update(EditarPropiedadCommand $editarPropiedad, Request $request, $id)
    {
        $id_propiedad = $id;
        $nombre = $request->RegistroNombre;
        $propiedad = $request->RegistroPropiedad;
        $dispositivoId = $request->RegistroDispositivoId;
        $editarPropiedad->run($id_propiedad, $nombre, $propiedad, $dispositivoId);
    }

    public function delete(BorrarPropiedadesCommand $borrarPropiedad, $id)
    {
        $borrarPropiedad->run($id);
    }
    public function vista()
    {
        return view('propiedades/listar');
    }
}
