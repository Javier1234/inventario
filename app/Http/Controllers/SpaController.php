<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class SpaController extends Controller
{
    public function index()
    {
        try {
            $index = Storage::disk('dist')->get('index.html');
            return $index;
        } catch (FileNotFoundException $exception) {
            return 'Missing front-end build';
        }
    }
}
