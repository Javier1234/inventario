<?php

namespace App\Http\Controllers;

use App\Dispositivo;
use Illuminate\Http\Request;
use Inventario\Dispositivos\Application\ListarDispositivos;
use Inventario\Dispositivos\Application\ListarDispositivo;
use Inventario\Dispositivos\Application\AnyadirDispositivo;
use Inventario\Dispositivos\Application\EditarDispositivo;
use Inventario\Dispositivos\Application\BorrarDispositivos;

class DispositivosController extends Controller
{
    public function index(ListarDispositivos $listarDispositivos, Request $request) //INYECCION DE DEPENDENCIAS
    {
        $dispositivos = $listarDispositivos->run();
        $coleccion = collect($dispositivos);
        return $coleccion->paginate($request->get('per_page'), $request->get('page'));
    }

    public function store(AnyadirDispositivo $anyadirDispositivo, Request $request)
    {
        $nombre = $request->nombre;
        $tipo = $request->tipo;
        $userId = $request->userId;
        $anyadirDispositivo->run($nombre, $tipo, $userId);
    }
    public function show(ListarDispositivo $listarDispositivo, $id)
    {
        $dispositivo = $listarDispositivo->run($id);
        return response()->api($dispositivo)->setStatusCode(200);
    }

    public function update(EditarDispositivo $editarDispositivo, Request $request, $id)
    {
        $id_dispositivo = $id;
        $nombre = $request->nombre;
        $tipo = $request->tipo;
        $userId = $request->userId;
        $editarDispositivo->run($id_dispositivo, $nombre, $tipo, $userId);
    }

    public function delete(BorrarDispositivos $borrarDispositivo, $id)
    {
        $borrarDispositivo->run($id);
    }
    public function vista()
    {
        return view('dispositivos/listar');
    }
}
