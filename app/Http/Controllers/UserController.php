<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Inventario\Usuarios\Application\ListarUsuariosCommand;
use Inventario\Usuarios\Application\ListarUsuarioCommand;
use Inventario\Usuarios\Application\AnyadirUsuarioCommand;
use Inventario\Usuarios\Application\EditarUsuarioCommand;
use Inventario\Usuarios\Application\BorrarUsuarioCommand;

class UserController extends Controller
{
    /**
     * Listar usuarios.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ListarUsuariosCommand $listarUsuarios, Request $request)
    {
        $usuarios = $listarUsuarios->run();
        $coleccion = collect($usuarios);
        return $coleccion->paginate($request->get('per_page'), $request->get('page'));
    }

    /**
     * Crea un nuevo usuario.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnyadirUsuarioCommand $anyadirUsuarios, Request $request)
    {
        $data = $request->all();

        $user = $anyadirUsuarios->run($data);
        return response()->api($user);
    }

    /**
     * Obtener usuario [id].
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $user->getPermissionsViaRoles();

        return response()->api($user);
    }

    public function obtenerUsuario(Request $request, ListarUsuarioCommand $listarUsuarioCommand, $idUsuario)
    {
        $user = $listarUsuarioCommand->run($idUsuario);

        return response()->api($user);
    }

    /** Actualizar usuario [all]
     * .
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditarUsuarioCommand $editarUsuario, Request $request, $idUsuario)
    {
        $data = $request->all();
        $data['id'] = $idUsuario;
        $user = $editarUsuario->run($data);
        return response()->api($user);
    }

    public function usuariosSelect(ListarUsuariosCommand $listarUsuarios, Request $request) 
    {
        $usuarios = $listarUsuarios->run();
        foreach ($usuarios as $usuario) {
            $datosSelect[$usuario['id']] = $usuario['name'];
        }
        return response()->json($datosSelect);
    }

    /**
     * Eliminar usuario [id].
     *
     * Queda pendiente definir como va a funcionar la eliminación de usuarios dentro de la plataforma.
     * Se borrará realmente el usuario y todo lo que cuelgue de él en cascada? o simplemente hacemos
     * una baja logica indicando un campo de eliminated_at con un timestamp.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(BorrarUsuarioCommand $borrarUsuario, Request $request, $idUsuario)
    {
        $usuarios = $borrarUsuario->run($idUsuario);
        return response()->json($usuarios);


    }

    /**
     * Usuario logueado.
     *
     */
    public function self()
    {
        $user = Auth::user();
        $user->withFullData();

        return response()->api($user);
    }
}
