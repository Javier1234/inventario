<?php

namespace App\Http\Responses;

class ApiResponse
{

    protected $content;
    protected $errors;
    protected $warnings;
    protected $status;

    public function __construct($content = null, $errors = [], $warnings = null, $status = 200)
    {
        $this->content = $content;
        $this->errors = $errors;
        $this->warnings = $warnings;
        $this->status = $status;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getWarnings()
    {
        return $this->warnings;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setContent($content)
    {
        return $this->content = $content;
    }

    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    public function setWarnings($warnings)
    {
        $this->warnings = $warnings;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function toArray()
    {
        return [
            "content"  => $this->content,
            "errors"   => $this->errors,
            "warnings" => $this->warnings,
            "status"   => $this->status
        ];
    }
}
