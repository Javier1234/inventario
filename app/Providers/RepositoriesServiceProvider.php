<?php

namespace App\Providers;

use App\Http\Kernel;
use Illuminate\Support\ServiceProvider;
use Inventario\Dispositivos\Domain\DispositivosRepositoryInterface;
use Inventario\Dispositivos\Infrastructure\DispositivosRepository;
use Inventario\Propiedades\Domain\PropiedadesRepositoryInterface;
use Inventario\Propiedades\Infrastructure\PropiedadesRepository;
use Inventario\Usuarios\Domain\UsuariosRepositoryInterface;
use Inventario\Usuarios\Infrastructure\UsuariosRepository;

class RepositoriesServiceProvider extends ServiceProvider
{
    // $singletons = [
    //     DispositivosRepositoryInterface::class => DispositivosRepository::class
    // ];
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Kernel $kernel)
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(DispositivosRepositoryInterface::class, DispositivosRepository::class);
        $this->app->bind(PropiedadesRepositoryInterface::class, PropiedadesRepository::class);
        $this->app->bind(UsuariosRepositoryInterface::class, UsuariosRepository::class);

    }
}
