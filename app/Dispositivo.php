<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dispositivo extends Model
{
    protected $table = 'dispositivos';

    protected $primaryKey = 'id';

    public function usuario()
    {
        return $this->belongsTo(User::class);
    }
}
