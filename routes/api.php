<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/dispositivos', 'DispositivosController@index'); //Listar todos los dispositivos
Route::get('/dispositivos/{id}', 'DispositivosController@show'); //Listar un dispositivo
Route::post('/dispositivos', 'DispositivosController@store'); //Anyadir un dispositivo
Route::put('/dispositivos/{id}', 'DispositivosController@update'); //Editar un dispositivo
Route::delete('/dispositivos/{id}', 'DispositivosController@delete'); //Borrar un dispositivo

Route::get('/propiedades/{id}', 'PropiedadesController@index'); //Listar todas las propiedades
Route::get('/propiedad/{id}', 'PropiedadesController@show'); //Listar una  propiedad
Route::post('/propiedades', 'PropiedadesController@store'); //Anyadir una propiedad
Route::put('/propiedades/{id}', 'PropiedadesController@update'); //Editar una propiedad
Route::delete('/propiedades/{id}', 'PropiedadesController@delete'); //Borrar una propiedad

Route::get('/usuariosSelect', 'UserController@usuariosSelect'); //Listar todos los usuarios
Route::get('/usuarios', 'UserController@index'); //Listar todos los usuarios
Route::get('/usuario/{id}', 'UserController@obtenerUsuario'); //Listar un usuario
Route::post('/usuarios', 'UserController@store'); //Anyadir un usuario 
Route::put('/usuarios/{id}', 'UserController@update'); //Editar un usuario
Route::delete('/usuarios/{id}', 'UserController@destroy'); //Borrar un usuario

Route::group(['middleware'   =>  'auth:api'], function () {

    Broadcast::routes();
    Route::get('/user', 'UserController@self');

    Route::group(['middleware' => 'throttle: 60,1'], function () {
        Route::post('logout', 'Auth\LoginController@logout');

        /**
         * Group for manage users
         */
        Route::group(['middleware' => 'can:manage_users'], function () {
            Route::get('/roles', 'RolesController@index');
            Route::get('/users', 'UserController@index');
            Route::get('/user/{user}', 'UserController@show');
            Route::post('/user', 'UserController@store');
            Route::put('/user/{user}', 'UserController@update');
            Route::delete('/user/{user}', 'UserController@destroy');
        });

        //Route::patch('settings/profile', 'Settings\ProfileController@update');
        //Route::patch('settings/password', 'Settings\PasswordController@update');
    });
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login');
    //Route::post('register', 'Auth\RegisterController@register');

    //Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    //Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    //Route::post('email/verify/{user}', 'Auth\VerificationController@verify')->name('verification.verify');
    //Route::post('email/resend', 'Auth\VerificationController@resend');

    //Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
    //Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');
});


Route::fallback('FallbackController');
